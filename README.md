# tembo-hub

p2p storage and communication hub for tembo ecosystem

# license

MIT

# usage

```bash
npm install --save tembo-hub
```
