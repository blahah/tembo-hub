var mkdirp = require('mkdirp').sync

var envpaths = require('env-paths')('tembo-hub')
Object.values(envpaths).forEach(mkdirp)

module.exports = envpaths
