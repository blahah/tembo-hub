var test = require('tape')
var fs = require('fs')

test ('env paths have been generated exist on fs', function (t) {
  var envpaths = require('../src/storage')

  var envkeys = ['config', 'data']
  
  envkeys.forEach(key => {
    var path = envpaths[key]

    t.ok(path)

    var exists = fs.existsSync(path)
    t.ok(exists)
  })
  
  t.end()
})
