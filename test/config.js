var test = require('tape')

test ('can write, read, and delete from config', function (t) {
  t.plan(2)
  var config = require('../src/config')

  var testval = '🐘'

  config.set('emoji', testval)
  t.equal(config.get('emoji'), testval)

  config.delete('emoji')
  t.notOk(config.get('emoji'))
})
