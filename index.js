// Auto-detects sane defaults based on your environment
// Uses Beaker's APIs if they are if they are available
// DatArchive is the same as Beaker
// https://beakerbrowser.com/docs/apis/dat
const {DatArchive} = require('dat-sdk/auto')

async function run () {
  const archive = await DatArchive.load('dat://dat.foundation')

  const someData = await archive.readFile('/dat.json', 'utf8')

  console.log('Dat foundation dat.json:', someData)

  const myArchive = await DatArchive.create({
      title: 'My Archive'
  })

  await myArchive.writeFile('/example.txt', 'Hello World!')

  const example = await myArchive.readFile('/example.txt')

  console.log('data we just wrote', example)

  return example
}

run().then(() => process.exit(1))
